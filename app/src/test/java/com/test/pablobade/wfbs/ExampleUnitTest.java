package com.test.pablobade.wfbs;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class ExampleUnitTest {

    @Test
    public void errorLogin() throws Exception {
        String rut = "19406667";
        String pass = "NOPASS";
        String expected = "Error!";

        SoapConnect soapConnect = Mockito.mock(SoapConnect.class, Mockito.RETURNS_DEEP_STUBS);
        Mockito.when(soapConnect.login(rut, pass)).thenReturn("Error!");

        String result = soapConnect.login(rut, pass);

        assertEquals(expected, result);
    }

    @Test
    public void successLogin() throws Exception {
        String rut = "19406667";
        String pass = "123456";
        String expected = "{'RESULT': {'USUARIO': RUT: '19406667', 'NOMBRE': 'PABLO BADE'}}";

        SoapConnect soapConnect = Mockito.mock(SoapConnect.class, Mockito.RETURNS_DEEP_STUBS);
        Mockito.when(soapConnect.login(rut, pass)).thenReturn("{'RESULT': {'USUARIO': RUT: '19406667', 'NOMBRE': 'PABLO BADE'}}");

        String result = soapConnect.login(rut, pass);

        assertEquals(expected, result);
    }

    @Test
    public void getFuncionarios() throws Exception {
        String expected = "{\n" +
                "  \"result\": [\n" +
                "    {\n" +
                "      \"RUT\": \"17084267\",\n" +
                "      \"DV\": \"7\",\n" +
                "      \"CONTRASEÑA\": \"123456\",\n" +
                "      \"ESTADO\": \"0\",\n" +
                "      \"JEFE_ID\": \"19406667\",\n" +
                "      \"CARGO_ID_CARGO\": 1.0,\n" +
                "      \"AREA_ID_AREA\": 1.0,\n" +
                "      \"NOMBRE\": \"RODRIGO FUENTES\",\n" +
                "      \"PERFIL_ID_PERFIL\": 1.0\n" +
                "    },\n" +
                "    {\n" +
                "      \"RUT\": \"19406667\",\n" +
                "      \"DV\": \"8\",\n" +
                "      \"CONTRASEÑA\": \"123456\",\n" +
                "      \"ESTADO\": \"1\",\n" +
                "      \"JEFE_ID\": \"00000000\",\n" +
                "      \"CARGO_ID_CARGO\": 1.0,\n" +
                "      \"AREA_ID_AREA\": 1.0,\n" +
                "      \"NOMBRE\": \"PABLO BADE\",\n" +
                "      \"PERFIL_ID_PERFIL\": 2.0\n" +
                "    },\n" +
                "    {\n" +
                "      \"RUT\": \"19004654\",\n" +
                "      \"DV\": \"0\",\n" +
                "      \"CONTRASEÑA\": \"123456\",\n" +
                "      \"ESTADO\": \"1\",\n" +
                "      \"JEFE_ID\": \"00000000\",\n" +
                "      \"CARGO_ID_CARGO\": 1.0,\n" +
                "      \"AREA_ID_AREA\": 1.0,\n" +
                "      \"NOMBRE\": \"AURORA LIZAMA\",\n" +
                "      \"PERFIL_ID_PERFIL\": 3.0\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        SoapConnect soapConnect = new SoapConnect();
//        SoapConnect soapConnect = Mockito.mock(SoapConnect.class, Mockito.RETURNS_DEEP_STUBS);
//        Mockito.when(soapConnect.login(rut, pass)).thenReturn("{'RESULT': {'USUARIO': RUT: '19406667', 'NOMBRE': 'PABLO BADE'}}");

        String result = soapConnect.getFuncionarios();

        assertEquals(expected, result);
    }

    @Test
    public void getInformeFuncionario() throws Exception {
        int rut = 17084267;
        String expected = "{\n" +
                "  \"result\": [\n" +
                "    {\n" +
                "      \"HABILIDAD\": \"Astucia\",\n" +
                "      \"NIVEL\": \"NIVEL 1\",\n" +
                "      \"NOTA\": 1.0\n" +
                "    }\n" +
                "  ]\n" +
                "}";
        SoapConnect soapConnect = new SoapConnect();
        String result = soapConnect.getInformeFuncionario(rut);
        assertEquals(expected, result);
    }
}