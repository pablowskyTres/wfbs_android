package com.test.pablobade.wfbs;

/**
 * Created by pablo on 25-10-17.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.test.pablobade.wfbs.Adapters.FuncionesAdapter;
import com.test.pablobade.wfbs.Adapters.HerramientasAdapter;
import com.test.pablobade.wfbs.Clases.Funciones;
import com.test.pablobade.wfbs.Clases.Herramienta;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class FragmentPerfil extends Fragment{

    private Funcionario funcionario;
    private TextView tvUsuario, tvArea, tvCargo, tvPerfil;
    private RecyclerView rvFunciones, rvHerramientas;
    private FuncionesAdapter funcionesAdapter;
    private HerramientasAdapter herramientasAdapter;
    private List<Funciones> funcionesList = new ArrayList<>();
    private List<Herramienta> herramientaList = new ArrayList<>();
    private Helper helper;

    public FragmentPerfil(){
    }

    private void initView(View v) throws ExecutionException, InterruptedException, JSONException {
        helper = new Helper();
        MainActivity a = (MainActivity) getActivity();
        funcionario = a.getFuncionario();
        tvUsuario = v.findViewById(R.id.tvUsuario);
        tvArea = v.findViewById(R.id.tvArea);
        tvCargo = v.findViewById(R.id.tvCargo);
        tvPerfil = v.findViewById(R.id.tvPerfil);

        tvUsuario.setText(a.getFuncionario().getNombre());
        tvArea.setText(a.getFuncionario().getArea());
        tvCargo.setText(a.getFuncionario().getCargo());
        tvPerfil.setText(a.getFuncionario().getPerfil());

        rvFunciones = (RecyclerView) v.findViewById(R.id.rvFunciones);
        rvHerramientas = (RecyclerView) v.findViewById(R.id.rvHerramientas);
        funcionesAdapter = new FuncionesAdapter(funcionesList);
        herramientasAdapter = new HerramientasAdapter(herramientaList);

        RecyclerView.LayoutManager mLayoutManagerFunciones = new LinearLayoutManager(getActivity().getApplicationContext());
        RecyclerView.LayoutManager mLayoutManagerHerramientas = new LinearLayoutManager(getActivity().getApplicationContext());
        rvFunciones.setLayoutManager(mLayoutManagerFunciones);
        rvFunciones.setItemAnimator(new DefaultItemAnimator());
        rvFunciones.setAdapter(funcionesAdapter);
        rvHerramientas.setLayoutManager(mLayoutManagerHerramientas);
        rvHerramientas.setItemAnimator(new DefaultItemAnimator());
        rvHerramientas.setAdapter(herramientasAdapter);
        loadData();
    }

    private void loadData() throws ExecutionException, InterruptedException, JSONException {
        SoapAsync soap = new SoapAsync();
        soap.execute(funcionario.getCargo_id());
        String res = soap.get();
        JSONObject jsonObject = new JSONObject(res);
        JSONArray jsonArray = jsonObject.getJSONArray("result");
        List<Funciones> funciones = helper.gson.fromJson(jsonArray.toString(), new TypeToken<List<Funciones>>(){}.getType());
        for(Funciones fun : funciones){
            funcionesList.add(fun);
        }
        funcionesAdapter.notifyDataSetChanged();

        SoapAsync2 soap2 = new SoapAsync2();
        soap2.execute(funcionario.getCargo_id());
        String res2 = soap2.get();
        JSONObject jsonObject2 = new JSONObject(res2);
        JSONArray jsonArray2 = jsonObject2.getJSONArray("result");
        List<Herramienta> herramientas = helper.gson.fromJson(jsonArray2.toString(), new TypeToken<List<Herramienta>>(){}.getType());
        for(Herramienta he : herramientas){
            herramientaList.add(he);
        }
        herramientasAdapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_perfil, container, false);
        try {
            initView(v);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return v;
    }

    class SoapAsync extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            SoapConnect soap = new SoapConnect();
            String res = soap.getFuncionesByCargoID(Integer.parseInt(params[0]));
            return res;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }
    }

    class SoapAsync2 extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            SoapConnect soap = new SoapConnect();
            String res = soap.getHerramientasByCargoID(Integer.parseInt(params[0]));
            return res;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }
    }

}
