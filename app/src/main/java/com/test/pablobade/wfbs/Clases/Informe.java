package com.test.pablobade.wfbs.Clases;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pablo on 05-12-17.
 */

public class Informe {
    @SerializedName("HABILIDAD")
    private String habilidad;
    @SerializedName("NIVEL")
    private String nivel;
    @SerializedName("NOTA")
    private String nota;

    public Informe() {
    }

    public String getHabilidad() {
        return habilidad;
    }

    public void setHabilidad(String habilidad) {
        this.habilidad = habilidad;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }
}
