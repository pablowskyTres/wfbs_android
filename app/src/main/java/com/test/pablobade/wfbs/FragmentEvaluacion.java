package com.test.pablobade.wfbs;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.test.pablobade.wfbs.Adapters.FuncionarioArrayAdapter;
import com.test.pablobade.wfbs.Adapters.InformeAdapter;
import com.test.pablobade.wfbs.Clases.Informe;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablo on 25-10-17.
 */

public class FragmentEvaluacion extends Fragment{

    private Funcionario funcionario;
    private Spinner spinner;
    private LinearLayout ficha;
    private InformeAdapter informeAdapter;
    private RecyclerView rvFicha;
    private List<Informe> informeList = new ArrayList<>();

    public FragmentEvaluacion(){
    }

    private FunciorioListResponse getFuncionarios() {
        Helper helper = new Helper();
        try {
            SoapAsync soap = new SoapAsync();
            soap.execute();
            String res = soap.get();
            FunciorioListResponse funcionarios = helper.gson.fromJson(res, FunciorioListResponse.class);
            for(Funcionario fun : funcionarios.getFuncionarioList()){
                Toast.makeText(getActivity().getApplicationContext(), fun.getNombre(), Toast.LENGTH_SHORT).show();
            }
            return funcionarios;
        }catch (Exception ex){
            Toast toast = Toast.makeText(getActivity().getApplicationContext(), ex.getMessage().toString(), Toast.LENGTH_SHORT);
            toast.show();
            return null;
        }
    }

    private InformeResponse getInformeFuncionario(String rut) {
        Helper helper = new Helper();
        try {
            SoapAsync2 soap = new SoapAsync2();
            soap.execute(rut);
            String res = soap.get();
            InformeResponse informeResponse = helper.gson.fromJson(res, InformeResponse.class);
            for(Informe inf : informeResponse.getInformeList()){
                Toast.makeText(getActivity().getApplicationContext(), inf.getHabilidad(), Toast.LENGTH_SHORT).show();
            }
            return informeResponse;
        }catch (Exception ex){
            Toast toast = Toast.makeText(getActivity().getApplicationContext(), ex.getMessage().toString(), Toast.LENGTH_SHORT);
            toast.show();
            return null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_evaluacion, container, false);
        rvFicha = v.findViewById(R.id.rvFicha);
        informeAdapter = new InformeAdapter(informeList);
        RecyclerView.LayoutManager mLayoutManagerInforme = new LinearLayoutManager(getActivity().getApplicationContext());
        rvFicha.setLayoutManager(mLayoutManagerInforme);
        rvFicha.setItemAnimator(new DefaultItemAnimator());
        rvFicha.setAdapter(informeAdapter);
        ficha = v.findViewById(R.id.lyFicha);
        ficha.setVisibility(View.GONE);
        spinner = v.findViewById(R.id.funcionarioSpinner);
        MainActivity a = (MainActivity) getActivity();
        funcionario = a.getFuncionario();
        if(funcionario.getPerfil_id() == 2 || funcionario.getPerfil_id() == 3){
            spinner.setVisibility(View.VISIBLE);
            final FunciorioListResponse funciorioListResponse = getFuncionarios();
            FuncionarioArrayAdapter arrayAdapter = new FuncionarioArrayAdapter(v.getContext(), R.layout.item_funcionario, funciorioListResponse.getFuncionarioList());
            arrayAdapter.notifyDataSetChanged();
            spinner.setAdapter(arrayAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    informeList.clear();
                    int rut = funciorioListResponse.getFuncionarioList().get(i).getRut();
                    InformeResponse informeResponse = getInformeFuncionario(rut+"");
                    for (Informe informe: informeResponse.getInformeList()
                            ) {
                        informeList.add(informe);
                    }
                    informeAdapter.notifyDataSetChanged();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            ficha.setVisibility(View.VISIBLE);
        }else{
            spinner.setVisibility(View.GONE);
            InformeResponse informeResponse = getInformeFuncionario(funcionario.getRut()+"");
            for (Informe informe: informeResponse.getInformeList()
                 ) {
                informeList.add(informe);
            }
            informeAdapter.notifyDataSetChanged();
            ficha.setVisibility(View.VISIBLE);
            Toast.makeText(getContext(), "Ficha Funcionario", Toast.LENGTH_SHORT).show();
        }
        return v;
    }

    class SoapAsync extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            SoapConnect soap = new SoapConnect();
            String res = soap.getFuncionarios();
            return res;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }
    }

    class SoapAsync2 extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            SoapConnect soap = new SoapConnect();
            String res = soap.getInformeFuncionario(Integer.parseInt(params[0]));
            return res;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }
    }

}
