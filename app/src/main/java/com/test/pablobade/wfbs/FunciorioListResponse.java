package com.test.pablobade.wfbs;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pablo on 05-12-17.
 */

public class FunciorioListResponse {
    @SerializedName("result")
    private List<Funcionario> funcionarioList;

    public FunciorioListResponse() {
    }

    public List<Funcionario> getFuncionarioList() {
        return funcionarioList;
    }

    public void setFuncionarioList(List<Funcionario> funcionarioList) {
        this.funcionarioList = funcionarioList;
    }
}
