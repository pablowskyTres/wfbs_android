package com.test.pablobade.wfbs.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.pablobade.wfbs.Clases.Herramienta;
import com.test.pablobade.wfbs.R;

import java.util.List;

/**
 * Created by pablo on 08-11-17.
 */

public class HerramientasAdapter extends RecyclerView.Adapter<HerramientasAdapter.MyViewHolder>{
    private List<Herramienta> herramientaList;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView herramienta, descripcion;

        public MyViewHolder(View v){
            super(v);
            herramienta = v.findViewById(R.id.herramienta);
            descripcion = v.findViewById(R.id.descripcion);
        }
    }

    public HerramientasAdapter(List<Herramienta> herramientaList) {
        this.herramientaList = herramientaList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_herramienta_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Herramienta herramienta = herramientaList.get(position);
        holder.herramienta.setText(herramienta.getHerramienta());
        holder.descripcion.setText(herramienta.getDescripcion());
    }

    @Override
    public int getItemCount() {
        return herramientaList.size();
    }
}