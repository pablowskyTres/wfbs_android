package com.test.pablobade.wfbs.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.pablobade.wfbs.Clases.Informe;
import com.test.pablobade.wfbs.R;

import java.util.List;

/**
 * Created by pablo on 05-12-17.
 */

public class InformeAdapter extends RecyclerView.Adapter<InformeAdapter.MyViewHolder> {
    private List<Informe> informeList;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView habilidad, nivel, nota;

        public MyViewHolder(View v){
            super(v);
            habilidad = v.findViewById(R.id.tvHabilidad);
            nivel = v.findViewById(R.id.tvNivel);
            nota = v.findViewById(R.id.tvNota);
        }
    }

    public InformeAdapter(List<Informe> informeList) {
        this.informeList = informeList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_informe, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Informe informe = informeList.get(position);
        holder.habilidad.setText(informe.getHabilidad());
        holder.nivel.setText(informe.getNivel());
        holder.nota.setText(informe.getNota());
    }

    @Override
    public int getItemCount() {
        return informeList.size();
    }
}
