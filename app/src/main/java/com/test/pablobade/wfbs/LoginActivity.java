package com.test.pablobade.wfbs;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    private EditText etUser, etPass;
    private Button btnLogin;
    private Helper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        helper = new Helper();
        initView();
    }

    private void initView(){
        etUser = (EditText) findViewById(R.id.etUser);
        etPass = (EditText) findViewById(R.id.etPass);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });


    }

    private void goMainActivity(Funcionario funcionario){
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra("usuario", (Serializable) funcionario);
        startActivity(i);
        finish();
    }

    private void login() {
        String user = etUser.getText().toString();
        String pass = etPass.getText().toString();
        try {
            SoapAsync soap = new SoapAsync();
            soap.execute(user, pass);
            String res = soap.get();
            JSONObject jsonObject = new JSONObject(res);
            JSONArray jsonArray = jsonObject.getJSONArray("result");
            List<Funcionario> funcionarios = helper.gson.fromJson(jsonArray.toString(), new TypeToken<List<Funcionario>>(){}.getType());
            for(Funcionario fun : funcionarios){
                Toast toast = Toast.makeText(this, "Bienvenido " + fun.getNombre(), Toast.LENGTH_SHORT);
                toast.show();
                goMainActivity(fun);
                break;
            }
        }catch (Exception ex){
            Toast toast = Toast.makeText(this, ex.getMessage().toString(), Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    class SoapAsync extends AsyncTask<String, Void, String>{

        ProgressDialog progress;

        @Override
        protected String doInBackground(String... params) {
            SoapConnect soap = new SoapConnect();
            String res = soap.login(params[0].toString(), params[1].toString());
            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            progress.hide();
        }

        @Override
        protected void onPreExecute() {
            progress = new ProgressDialog(LoginActivity.this);
            progress.setMessage("Espere por favor...");
            progress.setTitle("Cargando");
            progress.setMax(100);
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.show();
        }
    }

    class FuncionarioCollection {
        private List<Funcionario> funcionarios;

        public FuncionarioCollection() {

        }
    }
}
