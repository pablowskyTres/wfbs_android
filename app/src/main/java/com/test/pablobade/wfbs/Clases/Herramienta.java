package com.test.pablobade.wfbs.Clases;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pablo on 08-11-17.
 */

public class Herramienta {
    @SerializedName("HERRAMIENTA")
    private String herramienta;
    @SerializedName("DESCRIPCION")
    private String descripcion;

    public Herramienta() {
    }

    public String getHerramienta() {
        return herramienta;
    }

    public void setHerramienta(String herramienta) {
        this.herramienta = herramienta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
