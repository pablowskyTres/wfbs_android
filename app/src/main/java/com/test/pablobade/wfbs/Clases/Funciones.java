package com.test.pablobade.wfbs.Clases;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pablo on 06-11-17.
 */

public class Funciones {
    @SerializedName("FUNCION")
    private String funcion;
    @SerializedName("DESCRIPCION")
    private String descripcion;

    public Funciones() {
    }

    public Funciones(String funcion, String descripcion) {
        this.funcion = funcion;
        this.descripcion = descripcion;
    }

    public String getFuncion() {
        return funcion;
    }

    public void setFuncion(String funcion) {
        this.funcion = funcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
