package com.test.pablobade.wfbs;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.pablobade.wfbs.Adapters.InformeAdapter;
import com.test.pablobade.wfbs.Clases.Informe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablo on 25-10-17.
 */

public class FragmentInforme extends Fragment{

    private RecyclerView rvInforme;
    private InformeAdapter informeAdapter;
    private List<Informe> informeList = new ArrayList<>();

    public FragmentInforme(){

    }

    private InformeResponse getInformeAll() {
        Helper helper = new Helper();
        try {
            SoapAsync2 soap = new SoapAsync2();
            soap.execute();
            String res = soap.get();
            InformeResponse informeResponse = helper.gson.fromJson(res, InformeResponse.class);
            return informeResponse;
        }catch (Exception ex){
            return null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_informe, container, false);
        rvInforme = v.findViewById(R.id.rvInforme);
        RecyclerView.LayoutManager mLayoutManagerInforme = new LinearLayoutManager(getActivity().getApplicationContext());
        rvInforme.setLayoutManager(mLayoutManagerInforme);
        rvInforme.setItemAnimator(new DefaultItemAnimator());
        InformeResponse informeResponse = getInformeAll();
        List<Informe> informeListAsync = informeResponse.getInformeList();
        for (Informe informe: informeListAsync) {
            informeList.add(informe);
        }
        informeAdapter = new InformeAdapter(informeList);
        informeAdapter.notifyDataSetChanged();
        rvInforme.setAdapter(informeAdapter);
        return v;
    }

    class SoapAsync2 extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            SoapConnect soap = new SoapConnect();
            String res = soap.getInformeAll();
            return res;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }
    }
}
