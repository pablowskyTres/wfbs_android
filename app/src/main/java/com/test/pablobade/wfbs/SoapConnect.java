package com.test.pablobade.wfbs;

import android.os.AsyncTask;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.Console;
import java.util.ArrayList;

/**
 * Created by Pablo on 01-10-2017.
 */

public class SoapConnect{

    private String NAMESPACE = "http://wfbsws.ddns.net/";
    private String URL="http://wfbsws.ddns.net/WebService.asmx";

    public String login(String user, String pass){
        String METHOD_NAME = "LoginJson";
        String SOAP_ACTION = "http://wfbsws.ddns.net/LoginJson";

        String resultado;

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        request.addProperty("rut", user);
        request.addProperty("pass", pass);

        SoapSerializationEnvelope envelope =
                new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.dotNet = true;

        envelope.setOutputSoapObject(request);

        HttpTransportSE transporte = new HttpTransportSE(URL);

        try
        {
            transporte.call(SOAP_ACTION, envelope);
            SoapPrimitive res = (SoapPrimitive) envelope.getResponse();
            resultado = res.toString();
            return resultado;
        }
        catch (Exception e)
        {
            resultado = "Error!";
            return resultado;
        }
    }

    public String getFuncionarios(){
        String METHOD_NAME = "FuncionarioListJson";
        String SOAP_ACTION = "http://wfbsws.ddns.net/FuncionarioListJson";

        String resultado;

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        SoapSerializationEnvelope envelope =
                new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.dotNet = true;

        envelope.setOutputSoapObject(request);

        HttpTransportSE transporte = new HttpTransportSE(URL);

        try
        {
            transporte.call(SOAP_ACTION, envelope);
            SoapPrimitive res = (SoapPrimitive) envelope.getResponse();
            resultado = res.toString();
            return resultado;
        }
        catch (Exception e)
        {
            resultado = "Error!";
            return resultado;
        }
    }

    public String getFuncionesByCargoID(int cargo_id){
        String METHOD_NAME = "FuncionesByCargoIDJson";
        String SOAP_ACTION = "http://wfbsws.ddns.net/FuncionesByCargoIDJson";

        String resultado;

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        request.addProperty("cargo_id", cargo_id);

        SoapSerializationEnvelope envelope =
                new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.dotNet = true;

        envelope.setOutputSoapObject(request);

        HttpTransportSE transporte = new HttpTransportSE(URL);

        try
        {
            transporte.call(SOAP_ACTION, envelope);
            SoapPrimitive res = (SoapPrimitive) envelope.getResponse();
            resultado = res.toString();
            return resultado;
        }
        catch (Exception e)
        {
            resultado = "Error!";
            return resultado;
        }
    }

    public String getHerramientasByCargoID(int cargo_id){
        String METHOD_NAME = "HerramientaByCargoIDJson";
        String SOAP_ACTION = "http://wfbsws.ddns.net/HerramientaByCargoIDJson";

        String resultado;

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        request.addProperty("cargo_id", cargo_id);

        SoapSerializationEnvelope envelope =
                new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.dotNet = true;

        envelope.setOutputSoapObject(request);

        HttpTransportSE transporte = new HttpTransportSE(URL);

        try
        {
            transporte.call(SOAP_ACTION, envelope);
            SoapPrimitive res = (SoapPrimitive) envelope.getResponse();
            resultado = res.toString();
            return resultado;
        }
        catch (Exception e)
        {
            resultado = "Error!";
            return resultado;
        }
    }

    public String getInformeFuncionario(int rut) {
        String METHOD_NAME = "InformeFuncionarioJson";
        String SOAP_ACTION = "http://wfbsws.ddns.net/InformeFuncionarioJson";

        String resultado;

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        request.addProperty("rut", rut);

        SoapSerializationEnvelope envelope =
                new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.dotNet = true;

        envelope.setOutputSoapObject(request);

        HttpTransportSE transporte = new HttpTransportSE(URL);

        try
        {
            transporte.call(SOAP_ACTION, envelope);
            SoapPrimitive res = (SoapPrimitive) envelope.getResponse();
            resultado = res.toString();
            return resultado;
        }
        catch (Exception e)
        {
            resultado = "Error!";
            return resultado;
        }
    }

    public String getInformeAll() {
        String METHOD_NAME = "InformeFuncionarioAllListJson";
        String SOAP_ACTION = "http://wfbsws.ddns.net/InformeFuncionarioAllListJson";

        String resultado;

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        SoapSerializationEnvelope envelope =
                new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.dotNet = true;

        envelope.setOutputSoapObject(request);

        HttpTransportSE transporte = new HttpTransportSE(URL);

        try
        {
            transporte.call(SOAP_ACTION, envelope);
            SoapPrimitive res = (SoapPrimitive) envelope.getResponse();
            resultado = res.toString();
            return resultado;
        }
        catch (Exception e)
        {
            resultado = "Error!";
            return resultado;
        }
    }
}
