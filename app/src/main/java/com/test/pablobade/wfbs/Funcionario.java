package com.test.pablobade.wfbs;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pablo on 15-10-2017.
 */
public class Funcionario implements Serializable {
    @SerializedName("RUT")
    private int rut;
    @SerializedName("DV")
    private char dv;
    @SerializedName("NOMBRE")
    private String nombre;
    @SerializedName("CARGO")
    private String cargo;
    @SerializedName("CARGO_ID")
    private String cargo_id;
    @SerializedName("AREA")
    private String area;
    @SerializedName("AREA_ID")
    private String area_id;
    @SerializedName("SIGLA_AREA")
    private String sigla_area;
    @SerializedName("PERFIL_ID")
    private int perfil_id;
    @SerializedName("PERFIL")
    private String perfil;

    public Funcionario(){

    }

    public Funcionario(int rut, char dv, String nombre, String cargo, String cargo_id, String area, String area_id, String sigla_area, int perfil_id, String perfil) {
        this.rut = rut;
        this.dv = dv;
        this.nombre = nombre;
        this.cargo = cargo;
        this.cargo_id = cargo_id;
        this.area = area;
        this.area_id = area_id;
        this.sigla_area = sigla_area;
        this.perfil_id = perfil_id;
        this.perfil = perfil;
    }

    public int getRut() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut = rut;
    }

    public char getDv() {
        return dv;
    }

    public void setDv(char dv) {
        this.dv = dv;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getCargo_id() {
        return cargo_id;
    }

    public void setCargo_id(String cargo_id) {
        this.cargo_id = cargo_id;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getArea_id() {
        return area_id;
    }

    public void setArea_id(String area_id) {
        this.area_id = area_id;
    }

    public String getSigla_area() {
        return sigla_area;
    }

    public void setSigla_area(String sigla_area) {
        this.sigla_area = sigla_area;
    }

    public int getPerfil_id() {
        return perfil_id;
    }

    public void setPerfil_id(int perfil_id) {
        this.perfil_id = perfil_id;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    @Override
    public String toString() {
        return getRut()+"";
    }
}