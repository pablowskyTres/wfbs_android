package com.test.pablobade.wfbs.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.pablobade.wfbs.Clases.Funciones;
import com.test.pablobade.wfbs.R;

import java.util.List;

/**
 * Created by pablo on 06-11-17.
 */

public class FuncionesAdapter extends RecyclerView.Adapter<FuncionesAdapter.MyViewHolder>{
    private List<Funciones> funcionesList;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView funcion, descripcion;

        public MyViewHolder(View v){
            super(v);
            funcion = v.findViewById(R.id.funcion);
            descripcion = v.findViewById(R.id.descripcion);
        }
    }

    public FuncionesAdapter(List<Funciones> funcionesList) {
        this.funcionesList = funcionesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_funcion_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Funciones funcion = funcionesList.get(position);
        holder.funcion.setText(funcion.getFuncion());
        holder.descripcion.setText(funcion.getDescripcion());
    }

    @Override
    public int getItemCount() {
        return funcionesList.size();
    }
}
