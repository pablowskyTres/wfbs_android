package com.test.pablobade.wfbs;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private TabLayout tabs;
    private Toolbar toolbar;
    private Funcionario funcionario;
//    private TextView tvUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView(){
        funcionario = (Funcionario) getIntent().getSerializableExtra("usuario");
//        tvUsuario = (TextView) findViewById(R.id.tvUsuario);
//        tvUsuario.setText(funcionario.getNombre());
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setActionBar(toolbar);
        initTabs();
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        setupViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
    }

    private void setupViewPager(ViewPager viewPager) {
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentPerfil(), getString(R.string.fragment_perfil_title));
        adapter.addFragment(new FragmentEvaluacion(), getString(R.string.fragment_evaluacion_title));
        if (funcionario.getPerfil_id() == 3){
            adapter.addFragment(new FragmentInforme(), getString(R.string.fragment_informe_title));
        }
        viewPager.setAdapter(adapter);
    }

    private void initTabs(){
        try{
            tabs = (TabLayout) findViewById(R.id.tabs);
            tabs.addTab(tabs.newTab().setText("PERFIL"));
            tabs.addTab(tabs.newTab().setText("EVALUACION"));
            if (funcionario.getPerfil_id() == 3) {
                tabs.addTab(tabs.newTab().setText("INFORME"));
            }
            tabs.setTabMode(TabLayout.MODE_FIXED);
            tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    viewPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
        }catch (Exception ex){
            Toast.makeText(this, ex.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public Funcionario getFuncionario(){
        return this.funcionario;
    }
}
