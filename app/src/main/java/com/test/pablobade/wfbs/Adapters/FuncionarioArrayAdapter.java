package com.test.pablobade.wfbs.Adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.test.pablobade.wfbs.Funcionario;
import com.test.pablobade.wfbs.R;

import java.util.List;

/**
 * Created by pablo on 05-12-17.
 */

public class FuncionarioArrayAdapter extends ArrayAdapter {

    private Context context;
    private List<Funcionario> itemList;

    public FuncionarioArrayAdapter(Context context, int textViewResourceId,List<Funcionario> itemList) {
        super(context, textViewResourceId, itemList);
        this.context = context;
        this.itemList = itemList;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.item_funcionario, parent,
                false);
        TextView make = (TextView) row.findViewById(R.id.tvNombre);
        make.setText(itemList.get(position).getNombre());
        return row;
    }


    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.item_funcionario, parent,
                false);
        TextView make = (TextView) row.findViewById(R.id.tvNombre);
        make.setText(itemList.get(position).getNombre());
        return row;
    }
}
