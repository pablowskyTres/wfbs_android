package com.test.pablobade.wfbs;

/**
 * Created by pablo on 25-10-17.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class PagerAdapter extends FragmentStatePagerAdapter{
    List<Fragment> fragments;
    List<String> fragmentsTitle;

    public PagerAdapter(FragmentManager fm){
        super(fm);
        fragments = new ArrayList<>();
        fragmentsTitle = new ArrayList<>();
    }

    public void addFragment(Fragment fragment, String title){
        fragments.add(fragment);
        fragmentsTitle.add(title);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
