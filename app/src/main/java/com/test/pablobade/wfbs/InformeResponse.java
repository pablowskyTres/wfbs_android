package com.test.pablobade.wfbs;

import com.google.gson.annotations.SerializedName;
import com.test.pablobade.wfbs.Clases.Informe;

import java.util.List;

/**
 * Created by pablo on 05-12-17.
 */

public class InformeResponse {
    @SerializedName("result")
    private List<Informe> informeList;

    public InformeResponse() {
    }

    public List<Informe> getInformeList() {
        return informeList;
    }

    public void setInformeList(List<Informe> informeList) {
        this.informeList = informeList;
    }
}
